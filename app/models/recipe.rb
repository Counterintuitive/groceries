# frozen_string_literal: true

# == Schema Information
#
# Table name: recipes
#
#  id              :bigint           not null, primary key
#  rate            :string
#  author_tip      :string
#  budget          :string
#  prep_time       :string
#  ingredients     :string           default([]), is an Array
#  name            :string
#  author          :string
#  difficulty      :string
#  people_quantity :string
#  cook_time       :string
#  tags            :string           default([]), is an Array
#  total_time      :string
#  image           :string
#  nb_comments     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class Recipe < ApplicationRecord
  validates :name, presence: true
  validates :ingredients, presence: true

  scope :filter_by_ingredients, lambda { |filter_params, number_of_ingredients|
    array_to_string = "LOWER(array_to_string(ingredients, ' '))"
    query = []

    filter_params.each do |param|
      query.push("#{array_to_string} LIKE '%#{param.downcase}%'")
    end

    with_max_number_of_ingredients(number_of_ingredients).where(query.join(' AND '))
  }

  scope :with_max_number_of_ingredients, lambda { |number_of_ingredients|
                                           where('array_length(ingredients, 1) <= ?', number_of_ingredients)
                                         }
end
