# frozen_string_literal: true

module RecipesControllerDoc
  extend Apipie::DSL::Concern

  param :params, Hash, desc: 'Search and filter params' do
    param :filter, Array, desc: 'Filter recipes by ingredients'
  end

  api :GET, '/v1/recipes', 'Get a list of filtered recipes'
  def index; end
end
