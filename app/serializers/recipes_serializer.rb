# frozen_string_literal: true

class RecipesSerializer < ActiveModel::Serializer
  attributes :name, :ingredients, :difficulty, :image, :cook_time, :tags
end
