# frozen_string_literal: true

module Recipes
  class Filter
    def self.call(params)
      return [] if params[:filter].blank?

      @recipes = Recipe.all

      filter(params[:filter]) if params[:filter].present?

      @recipes
    end

    def self.filter(params)
      number_of_ingredients = params.size

      @recipes = @recipes.filter_by_ingredients(params, number_of_ingredients)
    end
  end
end
