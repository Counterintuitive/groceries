# frozen_string_literal: true

class RecipesController < ApplicationController
  include RecipesControllerDoc

  def index
    recipes = Recipes::Filter.call(params)

    render json: recipes, each_serializer: RecipesSerializer
  end
end
