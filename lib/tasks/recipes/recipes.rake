# frozen_string_literal: true

namespace :recipes do
  desc 'Import recipes from a given json file'
  # call using: recipes:import_recipes\['./lib/recipes.json'\]
  task :import_recipes, [:file_path] => :environment do |_task, args|
    require 'json'

    raise Errno::ENOENT if args.file_path.blank?

    file = File.read(args.file_path)
    file = "[#{file.chop.gsub!(/\n/, ', ')}]"

    imported_recipes = JSON.parse(file, { symbolize_names: true })

    imported_recipes.each do |recipe|
      recipe.merge!(created_at: Time.zone.now, updated_at: Time.zone.now)
    end

    puts "Taks started at #{Time.zone.now}"

    # rubocop:disable Rails/SkipsModelValidations
    Recipe.insert_all(imported_recipes)
    # rubocop:enable Rails/SkipsModelValidations

    puts "Taks finished at #{Time.zone.now}"

  rescue Errno::ENOENT
    puts 'Please pass a valid file_path as argument'
  end
end
