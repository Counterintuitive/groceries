# frozen_string_literal: true

Apipie.configure do |config|
  config.app_name                = 'GroceriesApi'
  config.api_base_url            = '/api'
  config.doc_base_url            = '/apipie'
  # where is your API defined?
  config.translate = false
  config.api_controllers_matcher = Rails.root.join('app/controllers/**/*.rb')
  config.show_all_examples = true
end
