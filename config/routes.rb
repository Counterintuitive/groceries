# frozen_string_literal: true

Rails.application.routes.draw do
  apipie
  scope :api do
    scope :v1, defaults: {format: :json} do
      resources :recipes, only: :index do
        get :ingredients, on: :collection
      end
    end
  end
end
