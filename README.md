# README

This is a Ruby on Rails API, using rails 6.1.4 and ruby 2.7.4

The API is hosted on heroku and can be found here https://pennylane-recipes-api.herokuapp.com/api/v1

Documentation for endpoints can be found here https://pennylane-recipes-api.herokuapp.com/apipie

The main purpose of the API is to return a list of recipes based on ingredients.
