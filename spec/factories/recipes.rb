# frozen_string_literal: true

# == Schema Information
#
# Table name: recipes
#
#  id              :bigint           not null, primary key
#  rate            :string
#  author_tip      :string
#  budget          :string
#  prep_time       :string
#  ingredients     :string           default([]), is an Array
#  name            :string
#  author          :string
#  difficulty      :string
#  people_quantity :string
#  cook_time       :string
#  tags            :string           default([]), is an Array
#  total_time      :string
#  image           :string
#  nb_comments     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
FactoryBot.define do
  factory :recipe do
    ingredients     { [Faker::Food.ingredient, Faker::Food.ingredient] }
    name            { Faker::Food.dish }
  end
end
