# frozen_string_literal: true

require 'rails_helper'
require 'rake'

Rails.application.load_tasks

describe 'import recipes' do
  subject { Rake::Task['recipes:import_recipes'].invoke(file_path) }

  after(:each) do
    Rake::Task['recipes:import_recipes'].reenable
  end

  context 'with arguments' do
    let(:file_path) { './lib/recipes.json' }

    it 'creates recipes' do
      expect { subject }.to(change { Recipe.count })
    end
  end

  context 'without valid arguments' do
    shared_examples 'an error message' do
      it 'outputs a message' do
        expect { subject }.to output("Please pass a valid file_path as argument\n").to_stdout
      end
    end

    context 'with missing file path' do
      let(:file_path) { '' }

      it_behaves_like 'an error message'
    end

    context 'with inexistent file path' do
      let(:file_path) { '/this-is-an-invalid-path' }

      it_behaves_like 'an error message'
    end
  end
end
