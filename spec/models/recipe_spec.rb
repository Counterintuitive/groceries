# frozen_string_literal: true

# == Schema Information
#
# Table name: recipes
#
#  id              :bigint           not null, primary key
#  rate            :string
#  author_tip      :string
#  budget          :string
#  prep_time       :string
#  ingredients     :string           default([]), is an Array
#  name            :string
#  author          :string
#  difficulty      :string
#  people_quantity :string
#  cook_time       :string
#  tags            :string           default([]), is an Array
#  total_time      :string
#  image           :string
#  nb_comments     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
require 'rails_helper'

RSpec.describe Recipe, type: :model do
  subject { create(:recipe) }

  it 'is not valid without name' do
    subject.name = nil

    expect(subject).to_not be_valid
  end

  it 'is not valid without ingredients' do
    subject.ingredients = []

    expect(subject).to_not be_valid
  end
end
