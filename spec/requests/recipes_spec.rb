# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Recipes', type: :request do
  describe 'GET /index' do
    subject { get '/api/v1/recipes', params: params }

    context 'without params' do
      let(:params) { {} }

      it 'returns an empty array', :show_in_doc, doc_title: 'Without params' do
        subject

        expect(response).to have_http_status(:ok)
        expect(json).to eq([])
      end
    end

    context 'with filter params' do
      context 'when one ingredient is found in several recipes' do
        before do
          create(:recipe, ingredients: %w[peanuts butter jelly])
          create(:recipe, ingredients: %w[peanuts butter])
          create(:recipe, ingredients: ['butter'])
          create(:recipe, ingredients: ['butter, salt'])
        end

        let(:params) { { filter: %w[butter salt] } }

        it 'returns recipes that have only the matching recipe', :show_in_doc, doc_title: 'With filter params' do
          subject

          expect(response).to have_http_status(:ok)
          expect(json.count).to eq(1)
        end
      end
    end
  end
end
